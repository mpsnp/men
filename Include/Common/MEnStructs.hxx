#ifndef _MEN_STRUCTS_H
#define _MEN_STRUCTS_H

#include "MEnGL.hxx"

namespace MEn
{
	struct TVertex3f
	{
		float x;
		float y;
		float z;
		
		TVertex3f();
		TVertex3f(float X, float Y, float Z);

		TVertex3f Normalize();
		float Length();
		void operator=(TVertex3f ARight);
		void operator+=(TVertex3f ARight);
		TVertex3f operator+(TVertex3f ARight);
		TVertex3f operator-(TVertex3f ARight);
		TVertex3f operator-();
		TVertex3f operator*(float ARight);
		TVertex3f operator^(TVertex3f ARight);
		float operator*(TVertex3f ARight);
	};

	typedef TVertex3f TNormal;

	struct TFace
	{
		int v1, v2, v3;
	};

	struct TMatrix4f
	{
		float m[4][4];

		TMatrix4f();
		TMatrix4f(float ADElement);
		TMatrix4f(float m00, float m11, float m22, float m33);
		TMatrix4f(float m00, float m01, float m02, float m03,
				  float m10, float m11, float m12, float m13,
				  float m20, float m21, float m22, float m23,
				  float m30, float m31, float m32, float m33 );

		TMatrix4f operator+(TMatrix4f& ARight);
		TMatrix4f operator-(TMatrix4f& ARight);
		TMatrix4f operator*(TMatrix4f& ARight);

		TMatrix4f Transpose();
	};

	struct TQuaternion
	{
		float x;
		float y;
		float z;
		float w;

		TQuaternion();
		TQuaternion(TVertex3f V, float W);
		TQuaternion(float X, float Y, float Z, float W);

		TMatrix4f GetRotationMatrix();
		float Norm();

		/// @brief Сопряженное
		TQuaternion operator!();
		TQuaternion operator+(TQuaternion& ARight);
		TQuaternion operator-(TQuaternion& ARight);
		TQuaternion operator*(TQuaternion& ARight);
	};
};

#endif
