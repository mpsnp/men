#ifndef _MEN_TYPES_H
#define _MEN_TYPES_H 

namespace MEn
{
	/// Engine base class.
	class IMEnBase
	{
		public:
			/// Just for correct destruction.
			virtual ~IMEnBase() {};
	};

	/*! @brief Типы сообщений лога ядра.
	 *
	 *  При получении фатальной ошибки, ядро завершает свою работу.
	 */
	enum E_LOG_MESSAGE_TYPE 
	{
		/// Заметка.
		ELMT_NOTE =		1,
		/// Предупреждение
		ELMT_WARNING =	2,
		/// Ошибка
		ELMT_ERROR =	4,
		/// Фатальная ошибка.
		ELMT_FATAL =	8
	};

	/*! @brief Флаги инициализации окна.
	 */
	enum E_WINDOW_INIT_FLAGS
	{
		/// Стандартные настройки.
		EWIF_DEFAULT =		0,
		/// Полноэкранный режим.
		EWIF_FULLSCREEN =	1,
		/// Окно, с невозможностью изменения размера.
		EWIF_CONSTRAINED =	2
	};

	enum E_ATTRIBUTE_TYPE
	{
		EAT_VERTEX
	};

	/*! @brief Типы подсистем.
	 */
	enum E_SUBSYSTEM_TYPE
	{
		/// Менеджер окон.
		EST_WINDOW_MANAGER = 1,
		/// Менеджер ресурсов.
		EST_RESOURCE_MANAGER,
		/// Подсистема рендера.
		EST_RENDER,
		/// Неизвестная подсистема.
		EST_UNKNOWN
	};

	/*! @brief Типы ресурсов.
	 */
	enum E_RESOURCE_TYPE
	{
		/// Окно
		ERT_WINDOW = 1,
		/// Игра
		ERT_GAME,
		/// Сцена
		ERT_SCENE,
		ERT_3D_OBJECT,
		ERT_3D_MESH,
		ERT_SHADER,
		ERT_CAMERA,
		ERT_UNKNOWN
	};

	/*! @brief Типы сообщений ввода.
	 */
	enum E_INPUT_MESSAGE_TYPE
	{
		EIMT_KEYBOARD = 1,
		EIMT_MOUSE,
		EIMT_JOYSTICK
	};

	/*! @brief Клавиши клавиатуры.
	 */
	enum E_KEYBOARD_KEYS
	{
		EKK_UNKNOWN =			-1,			
		EKK_SPACE =				32,
		EKK_APOSTROPHE =		39,  /* ' */
		EKK_COMMA =				44,  /* , */
		EKK_MINUS =				45,  /* - */
		EKK_PERIOD =			46,  /* . */
		EKK_SLASH =				47,  /* / */
		EKK_0 =					48,
		EKK_1 =					49,
		EKK_2 =					50,
		EKK_3 =					51,
		EKK_4 =					52,
		EKK_5 =					53,
		EKK_6 =					54,
		EKK_7 =					55,
		EKK_8 =					56,
		EKK_9 =					57,
		EKK_SEMICOLON =			59,  /* ; */
		EKK_EQUAL =				61,  /* =*/
		EKK_A =					65,
		EKK_B =					66,
		EKK_C =					67,
		EKK_D =					68,
		EKK_E =					69,
		EKK_F =					70,
		EKK_G =					71,
		EKK_H =					72,
		EKK_I =					73,
		EKK_J =					74,
		EKK_K =					75,
		EKK_L =					76,
		EKK_M =					77,
		EKK_N =					78,
		EKK_O =					79,
		EKK_P =					80,
		EKK_Q =					81,
		EKK_R =					82,
		EKK_S =					83,
		EKK_T =					84,
		EKK_U =					85,
		EKK_V =					86,
		EKK_W =					87,
		EKK_X =					88,
		EKK_Y =					89,
		EKK_Z =					90,
		EKK_LEFT_BRACKET =		91,  /* [ */
		EKK_BACKSLASH =			92,  /* \ */
		EKK_RIGHT_BRACKET =		93,  /* ] */
		EKK_GRAVE_ACCENT =		96,  /* ` */
		EKK_WORLD_1 =			161, /* non-US #1 */
		EKK_WORLD_2 =			162, /* non-US #2 */

		/* Function keys */
		EKK_ESCAPE =			256,
		EKK_ENTER =				257,
		EKK_TAB =				258,
		EKK_BACKSPACE =			259,
		EKK_INSERT =			260,
		EKK_DELETE =			261,
		EKK_RIGHT =				262,
		EKK_LEFT =				263,
		EKK_DOWN =				264,
		EKK_UP =				265,
		EKK_PAGE_UP =			266,
		EKK_PAGE_DOWN =			267,
		EKK_HOME =				268,
		EKK_END =				269,
		EKK_CAPS_LOCK =			280,
		EKK_SCROLL_LOCK =		281,
		EKK_NUM_LOCK =			282,
		EKK_PRINT_SCREEN =		283,
		EKK_PAUSE =				284,
		EKK_F1 =				290,
		EKK_F2 =				291,
		EKK_F3 =				292,
		EKK_F4 =				293,
		EKK_F5 =				294,
		EKK_F6 =				295,
		EKK_F7 =				296,
		EKK_F8 =				297,
		EKK_F9 =				298,
		EKK_F10 =				299,
		EKK_F11 =				300,
		EKK_F12 =				301,
		EKK_F13 =				302,
		EKK_F14 =				303,
		EKK_F15 =				304,
		EKK_F16 =				305,
		EKK_F17 =				306,
		EKK_F18 =				307,
		EKK_F19 =				308,
		EKK_F20 =				309,
		EKK_F21 =				310,
		EKK_F22 =				311,
		EKK_F23 =				312,
		EKK_F24 =				313,
		EKK_F25 =				314,
		EKK_KP_0 =				320,
		EKK_KP_1 =				321,
		EKK_KP_2 =				322,
		EKK_KP_3 =				323,
		EKK_KP_4 =				324,
		EKK_KP_5 =				325,
		EKK_KP_6 =				326,
		EKK_KP_7 =				327,
		EKK_KP_8 =				328,
		EKK_KP_9 =				329,
		EKK_KP_DECIMAL =		330,
		EKK_KP_DIVIDE =			331,
		EKK_KP_MULTIPLY =		332,
		EKK_KP_SUBTRACT =		333,
		EKK_KP_ADD =			334,
		EKK_KP_ENTER =			335,
		EKK_KP_EQUAL =			336,
		EKK_LEFT_SHIFT =		340,
		EKK_LEFT_CONTROL =		341,
		EKK_LEFT_ALT =			342,
		EKK_LEFT_SUPER =		343,
		EKK_RIGHT_SHIFT =		344,
		EKK_RIGHT_CONTROL =		345,
		EKK_RIGHT_ALT =			346,
		EKK_RIGHT_SUPER =		347,
		EKK_MENU =				348,
		EKK_LAST =				EKK_MENU
	};

	enum E_MODIFIERS
	{

	};

	/*! @brief Состояние клавиши клавиатуры
	 */
	enum E_KEY_STATE
	{
		/// Клавиша не нажата
		EKS_RELEASED = 0,
		/// Клавиша нажата
		EKS_PRESSED = 1,
		/// Двойное нажатие клавиши
		EKS_REPEAT = 2
	};

	/*! @brief Состояние клавиши клавиатуры
	 */
	enum E_BUTTON_STATE
	{
		/// Клавиша не нажата
		EBS_RELEASED = 0,
		/// Клавиша нажата
		EBS_PRESSED = 1
	};

	/*! @brief Кнопки мыши.
	 */
	enum E_MOUSE_BUTTONS
	{
		EMB_1 =			0,
		EMB_2 =			1,
		EMB_3 =			2,
		EMB_4 =			3,
		EMB_5 =			4,
		EMB_6 =			5,
		EMB_7 =			6,
		EMB_8 =			7,
		EMB_LAST =		EMB_8,
		EMB_LEFT =		EMB_1,
		EMB_RIGHT =		EMB_2,
		EMB_MIDDLE =	EMB_3
	};
};

#endif