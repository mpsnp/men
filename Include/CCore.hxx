#ifndef _CCORE_H
#define _CCORE_H

#include <string>
#include <fstream>
#include "MEnTypes.hxx"

/*! @brief Пространство имен движка.
 *
 *  Абсолютно все объекты движка являются членами этого пространства имен.
 *  @todo Перевести на делегаты, иначе выходит путанница.
 */

namespace MEn
{
	class CSubSystem;
	class CWindowManager;
	class CResourceManager;
	
	/*! @brief Класс ядра.
	 *
	 *  Ядро предназначено для управления окнами, ресурсами и сетью. 
	 *  Так-же осуществляет логгирование.
	 */
	class CCore : public IMEnBase
	{
		private:
			static CCore*		_pCore;
			int 				_iLoggingMask;
			std::fstream 		_LogStream;
			bool				_bLogOpened;
			CWindowManager*		_pWindowManager;
			CResourceManager*	_pResourceManager;
			bool				_bShouldTerminate;

			CCore();
			~CCore();
		public:
			/*! @brief Возвращает ядро.
			 *
			 *  Автоматически создает экземпляр ядра, если не создан.
			 *  @returns Возвращает указатель на ядро.
			 */
			static CCore* GetInstance();

			/*! @brief Удаляет ядро.
			 */
			static void FreeInstance();
		
			/*! @brief Returns true if engine and main loop should terminate.
			 */
			bool ShouldTerminate();

			/*! @brief Задает маску логгирования.
			 *
			 *  Например, если необходимо отключить ELMT_NOTE, 
			 *  то вызов метода будет таким:
			 *  @code
			 *  SetLoggingMask(ELMT_WARNING | ELMT_ERROR | ELMT_FATAL);
			 *  @endcode
			 *  @param[in] AMask Маска.
			 */
			void SetLoggingMask(int AMask);

			/*! @brief Добавляет в лог сообщение.
			 *
			 *  В зависимости от выбранной маски логгирования, добавляет (или не добавляет) в лог сообщение.
			 *  @param[in] AMessage Сообщение.
			 *  @param[in] AMessageType Тип сообщения.
			 */
			void AddToLog(std::string AMessage, E_LOG_MESSAGE_TYPE AMessageType);

			/*! @brief Возвращает подсистему.
			 *
			 *  Доступные подсистемы в ядре:
			 *  1. EST_WINDOW_MANAGER
			 *  2. EST_RESOURCE_MANAGER
			 *  
			 *  Если попытаться достать несуществующую подсистему, то ругнется сообщением ELMT_FATAL.
			 *  @param[in] AType Тип подсистемы.
			 */
			CSubSystem* GetSubSystem(E_SUBSYSTEM_TYPE AType);
	};
};

#endif