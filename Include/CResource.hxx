#ifndef _RESOURCE_H
#define _RESOURCE_H

#include "MEnTypes.hxx"

namespace MEn
{
	/*! @brief Базовый класс ресурсов.
	 *
	 *  Предназначен только для определения типа ресурса и универсальности хранения.
	 */
	class CResource : public IMEnBase
	{
		public:
			/*! @brief Возвращает тип ресурса.
			 *
			 *  @returns Тип ресурса E_RESOURCE_TYPE
			 */
			virtual E_RESOURCE_TYPE GetResourceType() = 0;
		
			/*! @brief Просто виртуальный деструктор, ну вы поняли зачем)
			 */
			virtual ~CResource() {};
	};
};

#endif