#ifndef _MEN_H
#define _MEN_H

#include <cstddef>
#include "CCore.hxx"
#include "CGame.hxx"
#include "CResourceManager.hxx"
#include "CWindow.hxx"
#include "CWindowManager.hxx"
#include "CScene.hxx"
#include "MEnTypes.hxx"
#include "C3dMesh.hxx"
#include "C3dObject.hxx"
#include "MEnStructs.hxx"
#include "CShader.hxx"
#include "CShaderProgram.hxx"
#include "CRender.hxx"
#include "CCamera.hxx"

#endif