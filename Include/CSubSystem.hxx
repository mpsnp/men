#ifndef _SUB_SYSTEM_H
#define _SUB_SYSTEM_H 

#include "MEnTypes.hxx"

namespace MEn
{
	/*! @brief Базовый класс подсистемы.
	 */
	class CSubSystem : public IMEnBase
	{
		public:
			CSubSystem();
			virtual ~CSubSystem();

			/*! @brief Тип подсистемы.
			 *  @returns Тип подсистемы.
			 */
			virtual E_SUBSYSTEM_TYPE GetType();

			/*! @brief Инициализационный метод.
			 *  Вызывается один раз в конструкторе.
			 */
			virtual void Init();

			/*! @brief Освобождающий метод.
			 *  Вызывается один раз в деструкторе.
			 */
			virtual void Free();
	};	
};

#endif