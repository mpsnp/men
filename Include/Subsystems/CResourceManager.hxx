#ifndef _RESOURCE_MANAGER_H
#define _RESOURCE_MANAGER_H

#include "CSubSystem.hxx"
#include "CResource.hxx"
#include "CResourceManipulator.hxx"
#include <string>
#include <vector>

namespace MEn
{
	/*! @brief Менеджер ресурсов.
	 *
	 *  В основном используется для загрузки и сохранения ресурсов. Для гибкости предоставляет возможность 
	 *  подключения собственных загрузчиков.
	 */
	class CResourceManager : public CSubSystem
	{
		private:
			std::vector<CResourceManipulator*>	_vManipulators;
			std::vector<CResource*>				_vResources;
		public:
			CResourceManager();
			~CResourceManager();
			void Free();

			/*! @brief Регистрирует манипулятор ресурсов.
			 *
			 *  @param AManipulator Манипулятор.
			 */
			void RegisterResourceManipulator(CResourceManipulator* AManipulator);

			/*! @brief Загружает ресурс из файла.
			 *
			 *  Если не находит подходящий манипулятор, то ругается в лог.
			 *  @param AFileName Имя файла.
			 *  @returns Загруженный ресурс или NULL, если манипулятор не найден.
			 */
			CResource* LoadResourceFromFile(std::string AFileName);

			/*! @brief Сохраняет ресурс в файл.
			 *
			 *  Если не находит подходящий манипулятор, то ругается в лог.
			 *  @param AResource Сохраняемый ресурс.
			 *  @param AFileName Имя файла.
			 */
			void SaveResourceToFile(CResource* AResource, std::string AFileName);
	};
};

#endif