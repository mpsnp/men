#ifndef _WINDOW_MANAGER_H
#define _WINDOW_MANAGER_H

#include <vector>
#include "CSubSystem.hxx"
#include "CWindow.hxx"
#include "MEnTypes.hxx"

namespace MEn
{
	/*! @brief Менеджер окон.
	 *
	 *  Необходим для управления окнами приложения, создания и корректного закрытия окон при выходе.
	 *  А так же инициализации взаимодействия с системой.
	 */
	class CWindowManager: public CSubSystem
	{
		private:
			std::vector<CWindow*> _pWindows;

			static void _ErrorCallback(int AError, const char* ADescription);
		public:
		
			CWindowManager();

			~CWindowManager();

			void Init();

			void Free();

			E_SUBSYSTEM_TYPE GetType();

			/*! @brief Создает окно.
			 *
			 *  Создает новое окно и возвращает его указатель.
			 *  @param[in] AWidth Ширина окна в пикселях.
			 *  @param[in] AHeight Высота окна в пикселях.
			 *  @param[in] ATitle Заголовок окна.
			 *  @param[in] AFlags Флаги инициализации окна.
			 *  @returns Указатель на объект окна CWindow.
			 */
			CWindow* CreateWindow(int AWidth, int AHeight, std::string ATitle, E_WINDOW_INIT_FLAGS AFlags);

			/*! @brief Возвращает окно.
			 *
			 *  Поиск производится по заголовку.
			 *  @param ATitle Искомый заголовок окна.
			 *  @returns Указатель на окно, если нашел, иначе NULL.
			 */
			CWindow* GetWindowByTitle(std::string ATitle);

			/*! @brief Удаляет окно и все его содержимое.
			 *
			 *  @param AWindow Удаляемое окно.
			 */
			void DeleteWindow(CWindow* AWindow);
	};
};

#endif