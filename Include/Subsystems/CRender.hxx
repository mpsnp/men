#ifndef _RENDER_SUBSYSTEM_H
#define _RENDER_SUBSYSTEM_H

#include "CSubSystem.hxx"
#include "CResource.hxx"
#include "MEnStructs.hxx"

namespace MEn
{
	class CShaderProgram;
	
	class CRender : public CSubSystem
	{
		private:
			static CRender* _pSingle;

			float _cGLSLVersion;

			GLuint _MVPLocation;

			TMatrix4f _MVP;
			TMatrix4f* _ModelTransform;
			TMatrix4f* _CameraTransform;

			CShaderProgram* _ActiveShaderProgram;

			CRender();
			~CRender();
		public:
			static CRender* GetInstance();
			static void FreeInstance();

			void LoadResourceToGPU(CResource* AResource);
			void UseShaderProgram(CShaderProgram* AProgram);
			void Render(CResource* AResource);

			GLuint GetAttributeLocation(E_ATTRIBUTE_TYPE AnAttribute);

			void SetModelTransform(TMatrix4f* AModelTransform);
			void SetCameraTransform(TMatrix4f* ACameraTransform);
		
			E_SUBSYSTEM_TYPE GetType();
	};
};

#endif
