#ifndef _RESOURCE_MANIPULATOR_H
#define _RESOURCE_MANIPULATOR_H

#include <string>
#include "CResource.hxx"
#include "MEnTypes.hxx"

namespace MEn
{
	/*! @brief Базовый класс манипулятоа ресурсов.
	 *
	 *  Представляет собой собственно загрузчик или сохранитель конкретного типа ресурса.
	 *  По умолчанию не может загрузить или сохранить ничего. Методы должны быть частично переопределены.
	 */
	class CResourceManipulator : public IMEnBase
	{
		public:
			virtual ~CResourceManipulator() {};
		
			/*! @brief Может ли загрузить ресурс.
			 *  @param AFileName Имя тестируемого файла.
			 *  @returns true, если загрузка возможна, иначе false.
			 */
			virtual bool CanLoadResource(std::string AFileName){return false;};

			/*! @brief Может ли сохранить ресурс.
			 *  @param AResource Тестируемый ресурс.
			 *  @returns true, если сохранение возможно, иначе false.
			 */
			virtual bool CanSaveResource(CResource* AResource){return false;};

			/*! @brief Загружает ресурс.
			 *  @param AFileName Имя загружаемого файла.
			 *  @returns Загруженный ресурс.
			 */
			virtual CResource* LoadResourceFromFile(std::string AFileName){return NULL;};

			/*! @brief Сохраняет ресурс.
			 *  @param AResource Сохраняемый ресурс.
			 *  @param AFileName Имя сохряняемого файла.
			 */
			virtual void SaveResourceToFile(CResource* AResource, std::string AFileName){};
	};
};

#endif