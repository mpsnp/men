#ifndef _SHADER_PROGRAM_MANIPULATOR_H
#define _SHADER_PROGRAM_MANIPULATOR_H

#include "CResourceManipulator.hxx"

namespace MEn
{
	class CShaderProgramManipulator : public CResourceManipulator
	{
		public:
			virtual bool CanLoadResource(std::string AFileName);
			virtual CResource* LoadResourceFromFile(std::string AFileName);
	};
}

#endif