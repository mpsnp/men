#ifndef _C3D_OBJ_MANIPULATOR
#define _C3D_OBJ_MANIPULATOR

#include "CResourceManipulator.hxx"

namespace MEn
{
	class C3dObjManipulator : public CResourceManipulator
	{
		public:
			virtual bool CanLoadResource(std::string AFileName);
			virtual CResource* LoadResourceFromFile(std::string AFileName);
	};
}

#endif