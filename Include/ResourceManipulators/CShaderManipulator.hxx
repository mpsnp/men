#ifndef _SHADER_MANIPULATOR_H
#define _SHADER_MANIPULATOR_H

#include "CResourceManipulator.hxx"
#include "MEnGL.hxx"

namespace MEn
{
	class CShaderManipulator : public CResourceManipulator
	{
		private:
			int _GetLength(std::ifstream &AInput);
			GLenum _GetType(std::string AFileName);
		public:
			virtual bool CanLoadResource(std::string AFileName);
			virtual CResource* LoadResourceFromFile(std::string AFileName);
	};
}

#endif
