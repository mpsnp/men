#ifndef _GRAPHICS_RESOURCE_H
#define _GRAPHICS_RESOURCE_H

#include "CResource.hxx"

namespace MEn
{
	class CGraphicsResource : public CResource
	{
		public:
			virtual bool LoadToGPU() = 0;
			virtual bool AlreadyLoadedToGPU() = 0;
			//virtual bool Render() = 0;
	};
};

#endif