#ifndef _NODE_H
#define _NODE_H

#include <vector>
#include "CResource.hxx"

namespace MEn
{
	/*! @brief Базовый класс нода.
	 */
	class CNode : public CResource
	{
		private:
			CNode* _pParent;
			std::vector<CNode*> _vChildren;
		public:
			/*! @brief Метод отрисовки.
			 */
			virtual void Render();

			/*! @brief Добавляет дочерний нод.
			 */
			void AddChild(CNode* AChild);

			/*! @brief Удаляет дочерний нод.
			 */
			void RemoveChild(CNode* AChild);

			/*! @brief Возвращает родительский нод.
			 */
			CNode* GetParent();

			/*! @brief Возвращает родительский нод.
			 */
			void SetParent(CNode* AParent);
	};	
};

#endif