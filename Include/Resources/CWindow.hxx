#ifndef _WINDOW_H
#define	_WINDOW_H

#include <string>
#include "MEnTypes.hxx"
#include "CResource.hxx"
	
namespace MEn
{
	class CGame;

	/// @todo Constrained param
	/*! @brief Класс окна.
	 */
	class CWindow : public CResource
	{
		public:
			E_RESOURCE_TYPE GetResourceType();

			/*! @brief Создает окно с заданными параметрами.
			 *
			 *  @param[in] AWidth Ширина окна в пикселях.
			 *  @param[in] AHeight Высота окна в пикселях.
			 *  @param[in] ATitle Заголовок окна.
			 *  @param[in] AFlags Флаги инициализации окна.
			 */
			static CWindow* CreateWindow(int AWidth, int AHeight, std::string ATitle, E_WINDOW_INIT_FLAGS AFlags);
			//CWindow(int AWidth, int AHeight, std::string ATitle, E_WINDOW_INIT_FLAGS AFlags);

			/*! @brief Грохает окно.
			 */
			virtual ~CWindow() {};

			/*! @brief Задает объект игры.
			 *  
			 *  После задания, окно автоматически начинает основной цикл.
			 *  @param AGame Экземпляр класса игры.
			 */
			virtual void SetGameInstance(CGame* AGame) = 0;

			/*! @brief Устанавливает ширину окна.
			 */
			virtual void SetWidth(int AWidth) = 0;

			/*! @brief Устанавливает высоту окна.
			 */
			virtual void SetHeight(int AHeight) = 0;

			/*! @returns Ширину окна.
			 */
			virtual int GetWidth() = 0;

			/*! @returns Высоту окна.
			 */
			virtual int GetHeight() = 0;

			/*! @returns Ширину окна.
			 */
			virtual int GetFrameWidth() = 0;

			/*! @returns Высоту окна.
			 */
			virtual int GetFrameHeight() = 0;

			virtual void SetCursorPos(int AX, int AY) = 0;
	};	
};

#endif