#ifndef _GAME_H
#define _GAME_H

#include "CResource.hxx"

namespace MEn
{
	class CScene;
	class CWindow;

	/*! @brief Базовый класс игры.
	 */
	class CGame : public CResource
	{
		private:
			CScene* _pCurrentScene;
			CWindow* _pParentWindow;
			bool _bRunning;
		public:

			/*! @brief Инициализация игры.
			 *  
			 *  Исполняется один раз перед входом в основной цикл.
			 */
			virtual void Init();

			/*! @brief Метод для организации игровой логики.
			 *  
			 *  Исполняется перед каждым кадром.
			 */
			virtual void Process();

			/*! @brief Отрисовка игры.
			 *  
			 */
			virtual void Render();

			/*! @brief Освобождение игры.
			 *  
			 *  Исполняется один раз после выхода из основного цикла.
			 *  Нет нужды его переопределять, если использовались только стандартные 
			 *  объекты и память не выделялась динамически.
			 */
			virtual void Free();

			/*! @brief Устанавливает текущую сцену.
			 *  @param AScene Устанавливаемая сцена.
			 */
			void SetCurrentScene(CScene* AScene);

			/*! @brief Устанавливает окно, в котором запущена игра.
			 */
			void SetParentWindow(CWindow* AWindow);

			CScene* GetCurrentScene();

			/*! @return Окно, в котором запущена игра.
			 */
			CWindow* GetParentWindow();

			/*! @return Запущена ли игра.
			 */
			bool Running();

			/*! @brief Устанавлвает флаг, запущена ли игра.
			 *  @param IsRunning Запущена или нет.
			 */
			void Running(bool IsRunning);

			/*! @brief Keyboard callback.
			 *  @param AKey Pressed or released key.
			 *  @param AModifier Held modifiers (such as Ctrl+Shift).
			 *  @param AStste Action with key.
			 */
			virtual bool KeyboardCallback(E_KEYBOARD_KEYS AKey, E_MODIFIERS AModifier, E_KEY_STATE AStste);

			virtual bool MouseButtonCallback(E_MOUSE_BUTTONS AButton, E_MODIFIERS AModifiers, E_BUTTON_STATE AState);

			virtual bool MouseWheelScrollCallback(double AXOffset, double AYOffset);

			virtual bool CursorPositionCallback(double AX, double AY);

			E_RESOURCE_TYPE GetResourceType();
	};
};

#endif