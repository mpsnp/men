#ifndef _C3D_OBJECT_H
#define _C3D_OBJECT_H

#include "CNode.hxx"
#include "C3dMesh.hxx"

namespace MEn
{
	class C3dObject : public CNode
	{
		private:
			C3dMesh* 	_pMesh;

			TVertex3f 	_tPosition;
			TVertex3f	_tScale;
			TQuaternion	_tRotation;

			TMatrix4f	_tRotationMatrix;
			TMatrix4f	_tPositionMatrix;
			TMatrix4f	_tScaleMatrix;

			TMatrix4f	_tTransformMatrix;

			bool _bRotated;
			bool _bMoved;
			bool _bScaled;

			bool _bOriginalScale;

			void _RecalculateTransform();
		public:
			C3dObject();
			void Rotate(TQuaternion ARotation);
			void Move(TVertex3f APosition);
			void Scale(TVertex3f AScale);
			void Render();
			void SetMesh(C3dMesh* AMesh);
			TMatrix4f* GetTransform();
			E_RESOURCE_TYPE GetResourceType();
	};
}

#endif
