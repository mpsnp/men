#ifndef _CAMERA_H
#define _CAMERA_H

#include "CNode.hxx"
#include "MEnStructs.hxx"
#define _USE_MATH_DEFINES
#include <math.h>

#define DEFAULT_FOV M_PI/2
#define DEFAULT_ASPECT 16.0 / 9.0

namespace MEn
{
	class CCamera : public CNode
	{
		private:
			TMatrix4f _Projection;
			TMatrix4f _Translation;
			TMatrix4f _Rotation;

			TMatrix4f _Camera;

			TVertex3f _Position;
			TVertex3f _Target;
			TVertex3f _Up;

			float _VerticalAngle;
			float _VerticalAngleLimit;
			float _HorisontalAngle;

			float _Near;
			float _Far;
			float _AspectRatio;
			float _FOV;

			bool _bPositionRecalculate;
			bool _bRotationRecalculate;
			bool _bProjectionRecalculate;

			void _Recalculate();
		public:
			CCamera();
			~CCamera();

			void Render();
			void SetPosition(TVertex3f APosition);

			/// Тангаж
			void Pitch(float ADelta);
			/// Рысканье
			void Yaw(float ADelta);
			/// Крен
			/// @todo Закончить.
			/// 
			/// @warning Не работает
			void Roll(float ADelta);

			void SetAspectRatio(float AnAspectRatio);
			void SetFov(float AFOW);
			void SetNear(float ANear);
			void SetFar(float AFar);

			void MoveForward(float ADelta);
			void MoveRight(float ADelta);

			/// @todo Solve problem with horisontal angle.
			void LookTo(TVertex3f AnAim);

			void SetTrackingNode(CNode* ATrackingNode);
		
			TMatrix4f* GetTransform(bool ARecalculate);
			E_RESOURCE_TYPE GetResourceType();
	};
};

#endif