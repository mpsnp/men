#ifndef _SCENE_H
#define _SCENE_H

#include "CNode.hxx"

namespace MEn
{
	class CCamera;
	class CGame;
	/*! @brief Базовый класс для игровых сцен.
	 */
	class CScene : public CNode
	{
		private:
			bool _bPresented;
			bool _bPaused;
			CGame* _OwnerGame;
			CCamera* _ActiveCamera;
		public:
			CScene();
			virtual ~CScene();
			/*! @brief Инициализационный метод.
			 *  
			 *  Предназначен для установки стортовых параметров сцены.
			 */
			virtual void Init();

			/*! @brief Метод для организации логики сцены.
			 */
			virtual void Process();

			/*! @brief Метод для установки паузы.
			 */
			void SetPaused(bool APaused);

			/*! @brief Метод для проверки паузы.
			 */
			bool Paused();

			CGame* GetOwnerGame();

			bool Presented();
			void Presented(bool APresented, CGame* AnOwnerGame);

			void SetActiveCamera(CCamera* ACamera);
			CCamera* GetActiveCamera();

			/*! @brief Keyboard callback.
			 *  @param AKey Pressed or released key.
			 *  @param AModifier Held modifiers (such as Ctrl+Shift).
			 *  @param AStste Action with key.
			 */
			virtual bool KeyboardCallback(E_KEYBOARD_KEYS AKey, E_MODIFIERS AModifier, E_KEY_STATE AStste);

			virtual bool MouseButtonCallback(E_MOUSE_BUTTONS AButton, E_MODIFIERS AModifiers, E_BUTTON_STATE AState);

			virtual bool MouseWheelScrollCallback(double AXOffset, double AYOffset);
		
			virtual bool CursorPositionCallback(double AX, double AY);

			E_RESOURCE_TYPE GetResourceType();
	};
};

#endif