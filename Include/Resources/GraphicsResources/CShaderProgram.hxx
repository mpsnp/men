#ifndef _SHADER_PROGRAM_H
#define _SHADER_PROGRAM_H

#include "CGraphicsResource.hxx"
#include "MEnGL.hxx"
#include <vector>

namespace MEn
{
	class CShader;

	class CShaderProgram : public CGraphicsResource
	{
		private:
			std::vector<CShader*> _vShaders;
			GLuint _giProgram;
			bool _bLinked;

			void _LinkProgram();
		public:
			CShaderProgram();
			~CShaderProgram();
			void AddShader(CShader* AShader);
			/// @todo Remove if already linked.
			/// Check the quality of deletion.
			void RemoveShader(CShader* AShader);
			bool LoadToGPU();
			bool AlreadyLoadedToGPU();
			GLuint GetShaderProgramID();
			E_RESOURCE_TYPE GetResourceType();
	};
};

#endif