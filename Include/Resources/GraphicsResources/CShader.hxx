#ifndef _SHADER_H
#define _SHADER_H

#include "CGraphicsResource.hxx"
#include "MEnGL.hxx"

namespace MEn
{
	class CShaderProgram;
	
	class CShader : public CGraphicsResource
	{
		private:
			char* _pSource;
			int _iLength;
			GLuint _giShaderPointer;
			GLenum _eShaderType;
			bool _bCompiled;

			void _Compile();
		public:
			friend class CShaderProgram;
		
			CShader();
			CShader(char** ASource, int ALength, GLenum AType);
			~CShader();
		
			void SetSource(char** ASource, int ALength, GLenum AType);
			char** GetSource();
			int GetLength();

			bool LoadToGPU();
			bool AlreadyLoadedToGPU();		
		
			E_RESOURCE_TYPE GetResourceType();
	};
};

#endif
