#ifndef _C3D_MESH_H
#define _C3D_MESH_H

#include "CGraphicsResource.hxx"
#include "MEnStructs.hxx"

namespace MEn
{
	class C3dMesh : public CGraphicsResource
	{
		private:
			TVertex3f* _pVertexes;
			TFace* _pFaces;
			TNormal* _pNormals;
			int _iVertexCount;
			int _iFaceCount;
			int _iNormalCount;
			GLuint _VertexBuffer;
			GLuint _VertexAttrib;
			GLuint _ElementBuffer;
			bool _LoadedToGPU;
		public:
			C3dMesh();
			~C3dMesh();
			E_RESOURCE_TYPE GetResourceType();
			void SetVertexes(TVertex3f* AVertexes, int AVertexCount);
			void SetFaces(TFace* AFaces, int AFaceCount);
			void SetNormals(TNormal* ANormals, int ANormalsCount);
			bool Render();
			/**
			 * @todo Finish it.
			 */
			bool LoadToGPU();
			bool AlreadyLoadedToGPU();
			void CalculateNormals();
	};
}

#endif
