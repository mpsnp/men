# MEn #
It is a simple multiplatform lightweight game engine written in `C++`. But it isn't completed yet and is in the deep development stage.
This engine doesn't pretend to be popular or something else. I develop it just for fun and experience.
## Building
This project uses `cmake` for building. So on Unix system just type this commands (in `MEn` directory) to complile all tests and examples.

```
#!sh

mkdir Build
cd Build
cmake ..
make
```

  