#include "CCore.hxx"
#include "CWindowManager.hxx"
#include "CResourceManager.hxx"

namespace MEn
{
	CCore* CCore::_pCore;

	CCore::CCore()
	{
		_pWindowManager = NULL;
		_pResourceManager = NULL;
		_bShouldTerminate = false;
		_LogStream.setf(std::ios_base::right, std::ios_base::adjustfield);
		_LogStream.open("MEngineLog.txt", std::fstream::out | std::ios::trunc);
		_bLogOpened = _LogStream.is_open();
		SetLoggingMask(ELMT_ERROR | ELMT_NOTE | ELMT_WARNING | ELMT_FATAL);
		AddToLog("Engine started!", ELMT_NOTE);
	};

	CCore::~CCore()
	{
		AddToLog("Terminating engine!", ELMT_NOTE);
		_LogStream.close();
		_bLogOpened = _LogStream.is_open();
		
		if (_pWindowManager)
			delete _pWindowManager;
		if (_pResourceManager)
			delete _pResourceManager;
	};

	CCore* CCore::GetInstance()
	{
		if (!_pCore) _pCore = new CCore();
		return _pCore;
	};

	void CCore::FreeInstance()
	{
		if (_pCore) {
			delete _pCore;
			_pCore = NULL;
		};
	};
	
	bool CCore::ShouldTerminate()
	{
		return _bShouldTerminate;
	};
	
	void CCore::SetLoggingMask(int AMask)
	{
		_iLoggingMask = AMask;
	};

	void CCore::AddToLog(std::string AMessage, E_LOG_MESSAGE_TYPE AMessageType)
	{
		if ((_iLoggingMask & AMessageType) && (_bLogOpened))
		{
			switch (AMessageType)
			{
				case ELMT_ERROR: 
					_LogStream << "Error: ";
					break;
				case ELMT_FATAL: 
					_LogStream << "Fatal Error: ";
					_bShouldTerminate = true;
					break;
				case ELMT_NOTE:
					_LogStream << "Note: ";
					break;
				case ELMT_WARNING:
					_LogStream << "Warning: ";
					break;
				default:
					_LogStream << "Unknown message type: ";
					break;
			};
			_LogStream << AMessage << std::endl;
			_LogStream.flush();
		};
	};

	CSubSystem* CCore::GetSubSystem(E_SUBSYSTEM_TYPE AType)
	{
		CSubSystem* subSystem = NULL;
		switch(AType)
		{
			case EST_WINDOW_MANAGER:
				if (!_pWindowManager)
					_pWindowManager = new CWindowManager();
				subSystem = _pWindowManager;
				break;
			case EST_RESOURCE_MANAGER:
				if (!_pResourceManager)
					_pResourceManager = new CResourceManager();
				subSystem = _pResourceManager;
				break;
			default:
				AddToLog("You tried to get unknown subsystem!", ELMT_FATAL);
				break;
		}
		return subSystem;
	};
};