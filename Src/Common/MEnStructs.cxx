#include "MEnStructs.hxx"
#include <cstring>
#include <cmath>

MEn::TVertex3f::TVertex3f()
{
	x = 0;
	y = 0;
	z = 0;
};

MEn::TVertex3f::TVertex3f(float X, float Y, float Z)
{
	x = X;
	y = Y;
	z = Z;
};

float MEn::TVertex3f::Length()
{
	return sqrtf(x * x + y * y + z * z);
};

MEn::TVertex3f MEn::TVertex3f::Normalize()
{
	float l = Length();
	x /= l;
	y /= l;
	z /= l;
	return *this;
};

MEn::TVertex3f MEn::TVertex3f::operator ^ (TVertex3f ARight)
{
	return TVertex3f(y * ARight.z - z * ARight.y,
					 z * ARight.x - x * ARight.z,
					 x * ARight.y - y * ARight.x);
};

void MEn::TVertex3f::operator = (TVertex3f ARight)
{
	x = ARight.x;
	y = ARight.y;
	z = ARight.z;
};

void MEn::TVertex3f::operator += (TVertex3f ARight)
{
	x += ARight.x;
	y += ARight.y;
	z += ARight.z;
};

MEn::TVertex3f MEn::TVertex3f::operator + (TVertex3f ARight)
{
	return TVertex3f(x + ARight.x, y + ARight.y, z + ARight.z);
};

float MEn::TVertex3f::operator *(TVertex3f ARight)
{
	return x * ARight.x + y * ARight.y + z * ARight.z;
};

MEn::TVertex3f MEn::TVertex3f::operator - (TVertex3f ARight)
{
	return TVertex3f(x - ARight.x, y - ARight.y, z - ARight.z);
};

MEn::TVertex3f MEn::TVertex3f::operator - ()
{
	return TVertex3f(-x, -y, -z);
};

MEn::TVertex3f MEn::TVertex3f::operator * (float ARight)
{
	return TVertex3f(x * ARight, y * ARight, z * ARight);
};

MEn::TMatrix4f::TMatrix4f()
{
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			m[i][j] = 0;
};

MEn::TMatrix4f::TMatrix4f(float ADElement)
{
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			m[i][j] = 0;
	m[0][0] = ADElement;
	m[1][1] = ADElement;
	m[2][2] = ADElement;
	m[3][3] = ADElement;
};

MEn::TMatrix4f::TMatrix4f(float m00, float m11, float m22, float m33)
{
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			m[i][j] = 0;
	m[0][0] = m00;
	m[1][1] = m11;
	m[2][2] = m22;
	m[3][3] = m33;	
};

MEn::TMatrix4f::TMatrix4f(	float m00, float m01, float m02, float m03, 
							float m10, float m11, float m12, float m13,
							float m20, float m21, float m22, float m23,
							float m30, float m31, float m32, float m33 )
{
	m[0][0] = m00;	m[0][1] = m01;	m[0][2] = m02;	m[0][3] = m03;
	m[1][0] = m10;	m[1][1] = m11;	m[1][2] = m12;	m[1][3] = m13;
	m[2][0] = m20;	m[2][1] = m21;	m[2][2] = m22;	m[2][3] = m23;
	m[3][0] = m30;	m[3][1] = m31;	m[3][2] = m32;	m[3][3] = m33;
};

MEn::TMatrix4f MEn::TMatrix4f::operator + (TMatrix4f& ARight)
{
	TMatrix4f result = *this;
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			result.m[i][j] += ARight.m[i][j];
	return result;
};

MEn::TMatrix4f MEn::TMatrix4f::operator - (TMatrix4f& ARight)
{
	TMatrix4f result = *this;
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			result.m[i][j] -= ARight.m[i][j];
	return result;
};

MEn::TMatrix4f MEn::TMatrix4f::operator * (TMatrix4f& ARight)
{
	TMatrix4f result;
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			result.m[i][j] =	m[i][0] * ARight.m[0][j] +
								m[i][1] * ARight.m[1][j] +
								m[i][2] * ARight.m[2][j] +
								m[i][3] * ARight.m[3][j];
	return result;
};

MEn::TMatrix4f MEn::TMatrix4f::Transpose()
{
	float temp;
	for (int i = 0; i < 4; ++i)
		for (int j = 0; j < 4; ++j)
		{
			temp = m[i][j];
			m[i][j] = m[j][i];
			m[j][i] = temp;
		};
	return *this;
};

MEn::TQuaternion::TQuaternion()
{
	x = 0;
	y = 0;
	z = 0;
	w = 1;
};

MEn::TQuaternion::TQuaternion(MEn::TVertex3f V, float W)
{
	x = V.x;
	y = V.y;
	z = V.z;
	w = W;
};

MEn::TQuaternion::TQuaternion(float X, float Y, float Z, float W)
{
	x = X;
	y = Y;
	z = Z;
	w = W;
};

MEn::TQuaternion MEn::TQuaternion::operator ! ()
{
	return TQuaternion(-x, -y, -z, w);
};

MEn::TQuaternion MEn::TQuaternion::operator + (TQuaternion& Ar)
{
	return TQuaternion(x + Ar.x, y + Ar.y, z + Ar.z, w + Ar.w);
};

MEn::TQuaternion MEn::TQuaternion::operator - (TQuaternion& Ar)
{
	return TQuaternion(x - Ar.x, y - Ar.y, z - Ar.z, w - Ar.w);
};

MEn::TQuaternion MEn::TQuaternion::operator * (TQuaternion& Ar)
{
	return TQuaternion(w * Ar.x + x*Ar.w + y*Ar.z - z*Ar.y,
						w*Ar.y + y*Ar.w + z*Ar.x - x*Ar.z,
						w*Ar.z + z*Ar.w + x*Ar.y - y*Ar.x,
						w*Ar.w - x*Ar.x - y*Ar.y - z*Ar.z);
	//return TQuaternion(v ^ ARight.v + ARight.v * w + v * ARight.w, w * ARight.w - v * ARight.v);
};

inline float MEn::TQuaternion::Norm()
{
	return x * x + y * y + z * z + w * w;
};

MEn::TMatrix4f MEn::TQuaternion::GetRotationMatrix()
{
	TMatrix4f m(1);
	float wx, wy, wz, xx, yy, yz, xy, xz, zz, x2, y2, z2;
	float s  = 2.0f/Norm();

	x2 = x * s;	y2 = y * s;	z2 = z * s;
	xx = x * x2;	xy = x * y2;	xz = x * z2;
	yy = y * y2;	yz = y * z2;	zz = z * z2;
	wx = w * x2;	wy = w * y2;	wz = w * z2;

	m.m[0][0] = 1.0f - (yy + zz);
	m.m[1][0] = xy - wz;
	m.m[2][0] = xz + wy;

	m.m[0][1] = xy + wz;
	m.m[1][1] = 1.0f - (xx + zz);
	m.m[2][1] = yz - wx;

	m.m[0][2] = xz - wy;
	m.m[1][2] = yz + wx;
	m.m[2][2] = 1.0f - (xx + yy);
	return m;
};