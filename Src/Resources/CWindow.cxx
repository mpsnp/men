#include "CWindow.hxx"
#include "CGame.hxx"
#include "CScene.hxx"
#include "CCore.hxx"
#include "CRender.hxx"
#include "CCamera.hxx"
#include "MEnGL.hxx"

namespace MEn
{	
	class CGLFWWindow : public CWindow
	{
		static CGLFWWindow* _pSingleWindow;
		GLFWwindow* _pWindow;
		CGame* _pGame;
		CRender* _pRender;

		int _iTop;
		int _iLeft;
		int _iWidth;
		int _iHeight;
		int _iFrameWidth;
		int _iFrameHeight;

		bool _bFullscreen;
		bool _bConstrained;
		bool _bActive;
		bool _bHidden;

		std::string _sTitle;

		void _MainLoop();
		void _RecreateWindow();
		void _ResizeViewport();
		void _ResizeWindow();
		void _SetCallbacks();

	public:
		static CWindow* CreateWindow(int AWidth, int AHeight, std::string ATitle, E_WINDOW_INIT_FLAGS AFlags);
		// Callbacks
		static void ResizeCallback(GLFWwindow* AWindow, int AWidth, int AHeight);
		static void PositionCallback(GLFWwindow* AWindow, int ATop, int ALeft);
		static void KeyCallback(GLFWwindow* AWindow, int AKey, int AScancode, int AnAction, int AMods);
		static void MouseButtonCallback(GLFWwindow *AWindow, int AButton, int AnAction, int AMods);
		static void CursorPositionCallback(GLFWwindow *AWindow, double AX, double AY);
		static void MouseWheelScrollCallback(GLFWwindow *AWindow, double AXOffset, double AYOffset);


		CGLFWWindow(int AWidth, int AHeight, std::string ATitle, E_WINDOW_INIT_FLAGS AFlags);
		~CGLFWWindow();
		void SetGameInstance(CGame* AGame);
		
		void SetWidth(int AWidth);
		void SetHeight(int AHeight);
		
		int GetWidth();
		int GetHeight();
		int GetFrameWidth();
		int GetFrameHeight();
		void SetCursorPos(int AX, int AY);
	};

	void CGLFWWindow::SetCursorPos(int AX, int AY)
	{
		glfwSetCursorPos(_pWindow, AX, AY);
	};

	void CGLFWWindow::_ResizeViewport()
	{
		glViewport(0, 0, _iFrameWidth, _iFrameHeight);
		this->_pGame->GetCurrentScene()->GetActiveCamera()->SetAspectRatio((float)_iFrameWidth / (float)_iFrameHeight);
	};
	
	void CGLFWWindow::_ResizeWindow()
	{
		glfwSetWindowSize(_pWindow, _iWidth, _iHeight);
	}

	void CGLFWWindow::ResizeCallback(GLFWwindow *AWindow, int AWidth, int AHeight)
	{
		_pSingleWindow->_iWidth = AWidth;
		_pSingleWindow->_iHeight = AHeight;
		glfwGetFramebufferSize(AWindow, &_pSingleWindow->_iFrameWidth, &_pSingleWindow->_iFrameHeight);
		_pSingleWindow->_ResizeViewport();
	};
    
	void CGLFWWindow::PositionCallback(GLFWwindow *AWindow, int ATop, int ALeft)
	{
		_pSingleWindow->_iTop = ATop;
		_pSingleWindow->_iLeft = ALeft;
	};

	void CGLFWWindow::KeyCallback(GLFWwindow* AWindow, int AKey, int AScancode, int AnAction, int AMods)
	{
		_pSingleWindow->_pGame->KeyboardCallback((E_KEYBOARD_KEYS)AKey, (E_MODIFIERS)AMods, (E_KEY_STATE)AnAction);
	};

	void CGLFWWindow::MouseButtonCallback(GLFWwindow *AWindow, int AButton, int AnAction, int AMods)
	{
		_pSingleWindow->_pGame->MouseButtonCallback((E_MOUSE_BUTTONS)AButton, (E_MODIFIERS)AMods, (E_BUTTON_STATE)AnAction);
	};

	void CGLFWWindow::CursorPositionCallback(GLFWwindow *AWindow, double AX, double AY)
	{
		_pSingleWindow->_pGame->CursorPositionCallback(AX, AY);
	};

	void CGLFWWindow::MouseWheelScrollCallback(GLFWwindow *AWindow, double AXOffset, double AYOffset)
	{
		_pSingleWindow->_pGame->MouseWheelScrollCallback(AXOffset, AYOffset);
	};

	CWindow* CGLFWWindow::CreateWindow(int AWidth, int AHeight, std::string ATitle, E_WINDOW_INIT_FLAGS AFlags)
	{
		if (!_pSingleWindow)
			_pSingleWindow = new CGLFWWindow(AWidth, AHeight, ATitle, AFlags);
		return _pSingleWindow;
	};

	CWindow* CWindow::CreateWindow(int AWidth, int AHeight, std::string ATitle, E_WINDOW_INIT_FLAGS AFlags)
	{
		return CGLFWWindow::CreateWindow(AWidth, AHeight, ATitle, AFlags);
	};

	E_RESOURCE_TYPE CWindow::GetResourceType()
	{
		return ERT_WINDOW;
	};

	int CGLFWWindow::GetWidth()
	{
		return _iWidth;
	};
	
	int CGLFWWindow::GetHeight()
	{
		return _iHeight;
	};

	int CGLFWWindow::GetFrameWidth()
	{
		return _iFrameWidth;
	};
	
	int CGLFWWindow::GetFrameHeight()
	{
		return _iFrameHeight;
	};
	
	void CGLFWWindow::SetWidth(int AWidth)
	{
		_iWidth = AWidth;
		_ResizeWindow();
	};
	
	void CGLFWWindow::SetHeight(int AHeight)
	{
		_iHeight = AHeight;
		_ResizeWindow();
	};

	void CGLFWWindow::_SetCallbacks()
	{
		glfwSetWindowSizeCallback(_pWindow, ResizeCallback);
		glfwSetWindowPosCallback(_pWindow, PositionCallback);
		glfwSetKeyCallback(_pWindow, KeyCallback);
		glfwSetScrollCallback(_pWindow, MouseWheelScrollCallback);
		glfwSetCursorPosCallback(_pWindow, CursorPositionCallback);
		glfwSetMouseButtonCallback(_pWindow, MouseButtonCallback);
	};

	CGLFWWindow::CGLFWWindow(int AWidth, int AHeight, std::string ATitle, E_WINDOW_INIT_FLAGS AFlags)
	{
		_pGame = NULL;
		_pWindow = NULL;
		_iWidth = AWidth;
		_iHeight = AHeight;
		_bFullscreen = AFlags && EWIF_FULLSCREEN;
		if (_bFullscreen)
		{
			const GLFWvidmode* videoMode = glfwGetVideoMode(glfwGetPrimaryMonitor());
			_iWidth = videoMode->width;
			_iHeight = videoMode->height;
		};
		_bConstrained = AFlags && EWIF_CONSTRAINED;
		_bActive = true;
		_bHidden = false;
		_sTitle = ATitle;

		_RecreateWindow();
		_pRender = CRender::GetInstance();
	};

	CGLFWWindow::~CGLFWWindow()
	{
		glfwDestroyWindow(_pWindow);
		// TODO: Удалить ресурсы.
		if (_pGame)
			delete _pGame;
		CRender::FreeInstance();
	};

	void CGLFWWindow::SetGameInstance(CGame* AGame)
	{
		if (!AGame)
			CCore::GetInstance()->AddToLog("Trying to set NULL game instance", ELMT_FATAL);
		else 
		{
			_pGame = AGame;
			_pGame->SetParentWindow(this);
			_pGame->Init();
			_MainLoop();
			_pGame->Free();
		}
	};

	void CGLFWWindow::_RecreateWindow()
	{
		if (_pWindow)
			glfwDestroyWindow(_pWindow);
		if (_bFullscreen)
			_pWindow = glfwCreateWindow(_iWidth, _iHeight, _sTitle.c_str(), glfwGetPrimaryMonitor(), NULL);
		else
			_pWindow = glfwCreateWindow(_iWidth, _iHeight, _sTitle.c_str(), NULL, NULL);
		glfwMakeContextCurrent(_pWindow);

		if (glewInit() != GLEW_OK)
			CCore::GetInstance()->AddToLog("Cannot init glew", ELMT_FATAL);

		_SetCallbacks();
	};

	void CGLFWWindow::_MainLoop()
	{
		while (_pGame->Running() && !glfwWindowShouldClose(_pWindow) && !CCore::GetInstance()->ShouldTerminate())
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			glfwMakeContextCurrent(_pWindow);
			_pGame->Process();
			_pGame->Render();

			glfwSwapBuffers(_pWindow);
			glfwPollEvents();
		}
	};

	CGLFWWindow* CGLFWWindow::_pSingleWindow = NULL;
};