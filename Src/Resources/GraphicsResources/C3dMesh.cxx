#include "C3dMesh.hxx"
#include "MEnGL.hxx"
#include "CRender.hxx"

namespace MEn
{
	void C3dMesh::SetVertexes(MEn::TVertex3f *AVertexes, int AVertexCount)
	{
		if (_pVertexes)
			delete[] _pVertexes;
		_pVertexes = AVertexes;
		_iVertexCount = AVertexCount;
	};
	
	void C3dMesh::SetFaces(MEn::TFace *AFaces, int AFaceCount)
	{
		if (_pFaces)
			delete [] _pFaces;
		_pFaces = AFaces;
		_iFaceCount = AFaceCount;
	};
	
	void C3dMesh::SetNormals(TNormal* ANormals, int ANormalsCount)
	{
		if (_pNormals)
			delete [] _pNormals;
		_pNormals = ANormals;
		_iNormalCount = ANormalsCount;
	};
	
	void C3dMesh::CalculateNormals()
	{
		if (_pNormals)
			delete [] _pNormals;
		_iNormalCount = _iVertexCount;
		_pNormals = new TNormal[_iNormalCount];
		TNormal tempNormal;
		for (int i = 0; i < _iFaceCount; i++)
		{
			TVertex3f a = _pVertexes[_pFaces[i].v2] - _pVertexes[_pFaces[i].v1];
			TVertex3f b = _pVertexes[_pFaces[i].v3] - _pVertexes[_pFaces[i].v1];
			tempNormal = a ^ b;
			tempNormal.Normalize();
			_pNormals[_pFaces[i].v1] += tempNormal;
			_pNormals[_pFaces[i].v2] += tempNormal;
			_pNormals[_pFaces[i].v3] += tempNormal;
		}

		for (int i = 0; i < _iNormalCount; i++)
			_pNormals[i].Normalize();
	};
	
	bool C3dMesh::Render()
	{
		if (!_LoadedToGPU)
			CRender::GetInstance()->LoadResourceToGPU(this);

		glEnableVertexAttribArray(_VertexAttrib);
		glBindBuffer(GL_ARRAY_BUFFER, _VertexBuffer);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ElementBuffer);

		glDrawElements(GL_TRIANGLES, _iFaceCount * 3, GL_UNSIGNED_INT, 0);

		glDisableVertexAttribArray(_VertexAttrib);

		return true;
	};
	
	bool C3dMesh::LoadToGPU()
	{
		glGenBuffers(1, &_VertexBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, _VertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(TVertex3f) * _iVertexCount, _pVertexes, GL_STATIC_DRAW);

		glGenBuffers(1, &_ElementBuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ElementBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, _iFaceCount * sizeof(TFace), _pFaces, GL_STATIC_DRAW);

		_VertexAttrib = CRender::GetInstance()->GetAttributeLocation(EAT_VERTEX);

		_LoadedToGPU = true;
		return _LoadedToGPU;
	};
	
	bool C3dMesh::AlreadyLoadedToGPU()
	{
		return _LoadedToGPU;
	};
	
	E_RESOURCE_TYPE C3dMesh::GetResourceType()
	{
		return ERT_3D_MESH;
	};
	
	C3dMesh::C3dMesh()
	{
		_pVertexes = NULL;
		_pFaces = NULL;
		_LoadedToGPU = false;
	};
	
	C3dMesh::~C3dMesh()
	{
		if (_pVertexes)
			delete[] _pVertexes;
		if (_pFaces)
			delete [] _pFaces;
		if (_LoadedToGPU)
		{
			glDeleteBuffers(1, &_VertexBuffer);
			glDeleteBuffers(1, &_ElementBuffer);
		};
	};
};