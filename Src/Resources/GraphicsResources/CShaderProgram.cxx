#include "CShaderProgram.hxx"
#include "CShader.hxx"
#include "CCore.hxx"
#include <algorithm>

MEn::CShaderProgram::CShaderProgram()
{
	_bLinked = false;
	_vShaders.clear();
};

GLuint MEn::CShaderProgram::GetShaderProgramID()
{
	return _giProgram;
};

MEn::CShaderProgram::~CShaderProgram()
{
};

void MEn::CShaderProgram::AddShader(CShader* AShader)
{
	if (AShader)
		_vShaders.push_back(AShader);
};

void MEn::CShaderProgram::RemoveShader(CShader* AShader)
{
	if (!_bLinked)
	{
		auto shader = find(_vShaders.begin(), _vShaders.end(), AShader);
		delete *shader;
		_vShaders.erase(shader);
	};	
};

bool MEn::CShaderProgram::LoadToGPU()
{
	if (!_bLinked)
		_LinkProgram();
	return _bLinked;
};

MEn::E_RESOURCE_TYPE MEn::CShaderProgram::GetResourceType()
{
	return ERT_UNKNOWN;
};

void MEn::CShaderProgram::_LinkProgram()
{
	CCore::GetInstance()->AddToLog("Linking shader program.", ELMT_NOTE);

	bool success = true;
	for (auto shader : _vShaders)
		if (!shader->AlreadyLoadedToGPU())
			success = success && shader->LoadToGPU();
	if (success)
	{
		GLint Success = 0;

		_giProgram = glCreateProgram();
		for (auto shader : _vShaders)
			glAttachShader(_giProgram, shader->_giShaderPointer);
		
		glLinkProgram(_giProgram);
		glGetProgramiv(_giProgram, GL_LINK_STATUS, &Success);
		_bLinked = success;

		if (!_bLinked)
		{
			int InfoLogLength;
			glGetProgramiv(_giProgram, GL_INFO_LOG_LENGTH, &InfoLogLength);
			char* InfoLog = new char[InfoLogLength];
			if ( InfoLogLength > 0 )
			{
				std::string errorMsg = "Error linking shader program:\n";
				glGetProgramInfoLog(_giProgram, InfoLogLength, NULL, InfoLog);
				errorMsg += InfoLog;
				CCore::GetInstance()->AddToLog(errorMsg, ELMT_ERROR);
			}
			delete [] InfoLog;
		}
		
		glValidateProgram(_giProgram);
		glGetProgramiv(_giProgram, GL_VALIDATE_STATUS, &Success);
		
		_bLinked = _bLinked && Success;
	}
	if (_bLinked)
		CCore::GetInstance()->AddToLog("Successfully linked!", ELMT_NOTE);
	else
		CCore::GetInstance()->AddToLog("Error while linking shader program", ELMT_ERROR);
};

bool MEn::CShaderProgram::AlreadyLoadedToGPU()
{
	return _bLinked;
};
