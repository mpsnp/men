#include "CShader.hxx"
#include "CCore.hxx"

MEn::CShader::CShader()
{
	_pSource = nullptr;
	_iLength = 0;
};

MEn::CShader::CShader(char** ASource, int ALength, GLenum AType)
{
	_pSource = *ASource;
	_iLength = ALength;
	_eShaderType = AType;
	_giShaderPointer = 0;
	_bCompiled = false;
};

MEn::CShader::~CShader()
{
	if (_pSource) {
		delete [] _pSource;
		_pSource = nullptr;
	}
};

void MEn::CShader::SetSource(char **ASource, int ALength, GLenum AType)
{
	if (_pSource)
		delete [] _pSource;
	_pSource = *ASource;
	_iLength = ALength;
	_eShaderType = AType;
};

char** MEn::CShader::GetSource()
{
	return &_pSource;
};

int MEn::CShader::GetLength()
{
	return _iLength;
};

MEn::E_RESOURCE_TYPE MEn::CShader::GetResourceType()
{
	return ERT_SHADER;
};

void MEn::CShader::_Compile()
{
	CCore::GetInstance()->AddToLog("Compiling shader.", ELMT_NOTE);
	
	_giShaderPointer = glCreateShader(_eShaderType);

	glShaderSource(_giShaderPointer, 1, (const char**)&_pSource , NULL);
	
	glCompileShader(_giShaderPointer);
	
	GLint success;
	glGetShaderiv(_giShaderPointer, GL_COMPILE_STATUS, &success);
	if (success)
	{
		_bCompiled = true;
		CCore::GetInstance()->AddToLog("Successfully compiled.", ELMT_NOTE);
	}
	else
	{
		int InfoLogLength;
		glGetShaderiv(_giShaderPointer, GL_INFO_LOG_LENGTH, &InfoLogLength);

		char* InfoLog = new char[InfoLogLength];

		glGetShaderInfoLog(_giShaderPointer, InfoLogLength, NULL, InfoLog);
		std::string errorMsg = "Error compiling shader:\n";
		errorMsg += InfoLog;
		CCore::GetInstance()->AddToLog(errorMsg, ELMT_ERROR);
		
		glDeleteShader(_giShaderPointer);
		_giShaderPointer = 0;
		_bCompiled = false;
		delete [] InfoLog;
	};
};

bool MEn::CShader::LoadToGPU()
{
	if (_bCompiled)
		CCore::GetInstance()->AddToLog("Trying to load to GPU second time.", ELMT_WARNING);
	else
		_Compile();
	return _bCompiled;
};

bool MEn::CShader::AlreadyLoadedToGPU()
{
	return _bCompiled;
};
