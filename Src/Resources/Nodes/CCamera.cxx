#include "CCamera.hxx"
#include "CRender.hxx"

MEn::CCamera::CCamera()
{
	_FOV = DEFAULT_FOV;
	_AspectRatio = DEFAULT_ASPECT;
	_bPositionRecalculate = true;
	_bRotationRecalculate = true;
	_bProjectionRecalculate = true;

	_VerticalAngle = 0;
	_VerticalAngleLimit = M_PI_2 * 0.99;
	_HorisontalAngle = 0;

	_Position = TVertex3f(-6,0,0);
	_Up = TVertex3f(0,0,1);

	_Near = 0.01;
	_Far = 1000;
	_FOV = M_PI / 4;

	_Projection = TMatrix4f(1);
	_Translation = TMatrix4f(1);
	_Rotation = TMatrix4f(1);

	_Camera = TMatrix4f(1);

	_Recalculate();
};

MEn::CCamera::~CCamera()
{

};

MEn::E_RESOURCE_TYPE MEn::CCamera::GetResourceType()
{
	return ERT_CAMERA;
};

void MEn::CCamera::SetPosition(TVertex3f APosition)
{
	_Position = APosition;
	_bPositionRecalculate = true;
};

void MEn::CCamera::Pitch(float ADelta)
{
	_VerticalAngle += ADelta;
	if (_VerticalAngle > _VerticalAngleLimit)
		_VerticalAngle = _VerticalAngleLimit;
	if (_VerticalAngle < -_VerticalAngleLimit)
		_VerticalAngle = -_VerticalAngleLimit;
	_bRotationRecalculate = true;
};

void MEn::CCamera::SetAspectRatio(float AnAspectRatio)
{
	_AspectRatio = AnAspectRatio;
	_bProjectionRecalculate = true;
};

void MEn::CCamera::LookTo(TVertex3f AnAim)
{
	_HorisontalAngle = atan((AnAim.y - _Position.y) / (AnAim.x - _Position.x));
	_VerticalAngle = asin((AnAim.z - _Position.z) / (AnAim - _Position).Length());
	_bRotationRecalculate = true;
};

void MEn::CCamera::Yaw(float ADelta)
{
	_HorisontalAngle += ADelta;
	_bRotationRecalculate = true;
};

void MEn::CCamera::Roll(float ADelta)
{
	
};

void MEn::CCamera::MoveForward(float ADelta)
{
	TVertex3f t = _Target;
	t.Normalize();
	t = t * ADelta;
	_Position = _Position + t;
	_bPositionRecalculate = true;
};

void MEn::CCamera::MoveRight(float ADelta)
{
	TVertex3f t = _Up ^ _Target;
	t.Normalize();
	t = t * ADelta;
	_Position = _Position + t;
	_bPositionRecalculate = true;
};

void MEn::CCamera::Render()
{
	CRender::GetInstance()->SetCameraTransform(GetTransform(true));
};

void MEn::CCamera::_Recalculate()
{
	if (_bPositionRecalculate)
	{
		_Translation.m[0][3] = -_Position.x;
		_Translation.m[1][3] = -_Position.y;
		_Translation.m[2][3] = -_Position.z;
	};

	if (_bRotationRecalculate)
	{
		_Target = TVertex3f(cos(_HorisontalAngle) * cos(_VerticalAngle), sin(_HorisontalAngle) * cos(_VerticalAngle), sin(_VerticalAngle));
		TVertex3f T = _Target;
		TVertex3f U = _Up;
		TVertex3f R = U ^ T;
		R.Normalize();
		U = T ^ R;
		U.Normalize();

		_Rotation.m[0][0] = R.x;   _Rotation.m[0][1] = R.y;   _Rotation.m[0][2] = R.z;   _Rotation.m[0][3] = 0.0f;
		_Rotation.m[1][0] = U.x;   _Rotation.m[1][1] = U.y;   _Rotation.m[1][2] = U.z;   _Rotation.m[1][3] = 0.0f;
		_Rotation.m[2][0] = T.x;   _Rotation.m[2][1] = T.y;   _Rotation.m[2][2] = T.z;   _Rotation.m[2][3] = 0.0f;
		_Rotation.m[3][0] = 0.0f;  _Rotation.m[3][1] = 0.0f;  _Rotation.m[3][2] = 0.0f;  _Rotation.m[3][3] = 1.0f;
	};

	if (_bProjectionRecalculate)
	{
		const float ar = _AspectRatio;
		const float zRange = _Near - _Far;
		const float THF = tanf(_FOV / 2.0);

		_Projection.m[0][0] = 1.0f/(THF * ar);	_Projection.m[0][1] = 0.0f;		_Projection.m[0][2] = 0.0f;					_Projection.m[0][3] = 0.0;
		_Projection.m[1][0] = 0.0f;				_Projection.m[1][1] = 1.0f/THF;	_Projection.m[1][2] = 0.0f;					_Projection.m[1][3] = 0.0;
		_Projection.m[2][0] = 0.0f;				_Projection.m[2][1] = 0.0f;		_Projection.m[2][2] = (-_Near-_Far)/zRange;	_Projection.m[2][3] = 2.0f * _Far*_Near/zRange;
		_Projection.m[3][0] = 0.0f;				_Projection.m[3][1] = 0.0f;		_Projection.m[3][2] = 1.0f;					_Projection.m[3][3] = 0.0;
	};

	if (_bPositionRecalculate || _bRotationRecalculate || _bProjectionRecalculate)
	{
		_bProjectionRecalculate = false;
		_bRotationRecalculate = false;
		_bPositionRecalculate = false;
		_Camera = _Projection * _Rotation * _Translation;
	};
};

MEn::TMatrix4f* MEn::CCamera::GetTransform(bool ARecalculate)
{
	if (ARecalculate)
		_Recalculate();
	return &_Camera;
};
