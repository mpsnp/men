#include "C3dObject.hxx"
#include "CRender.hxx"

namespace MEn
{
	void C3dObject::Render()
	{
		CRender* render = CRender::GetInstance();
		render->SetModelTransform(GetTransform());
		if (_pMesh)
			render->Render(_pMesh);
	};
	
	void C3dObject::_RecalculateTransform()
	{
		if (_bMoved)
		{
			_tPositionMatrix.m[0][3] = _tPosition.x;
			_tPositionMatrix.m[1][3] = _tPosition.y;
			_tPositionMatrix.m[2][3] = _tPosition.z;
		};

		if (_bRotated)
			_tRotationMatrix = _tRotation.GetRotationMatrix();

		if (_bScaled && _bOriginalScale)
			_tScaleMatrix = TMatrix4f(_tScale.x, _tScale.y, _tScale.z, 1);

		_bMoved = _bRotated = _bScaled = false;

		if (_bOriginalScale)
			_tTransformMatrix = _tPositionMatrix * _tRotationMatrix;
		else
			_tTransformMatrix = _tPositionMatrix * _tRotationMatrix * _tScaleMatrix;
	};
	
	void C3dObject::Move(TVertex3f APosition)
	{
		_tPosition = APosition;
		_bMoved = true;
	};

	void C3dObject::Rotate(TQuaternion ARotation)
	{
		_tRotation = _tRotation * ARotation;
		_bRotated = true;
	};

	void C3dObject::Scale(TVertex3f AScale)
	{
		_tScale = AScale;
		_bScaled = true;
	};

	TMatrix4f* C3dObject::GetTransform()
	{
		if (_bMoved || _bRotated)
			_RecalculateTransform();
		return &_tTransformMatrix;
	};
	
	C3dObject::C3dObject()
	: _tTransformMatrix(1), _tPositionMatrix(1), _tRotationMatrix(1), _tScaleMatrix(1)
	{
		_bMoved = _bRotated = _bScaled = false;
		_bOriginalScale = true;
		_tPosition = TVertex3f();
		_tScale = TVertex3f(1, 1, 1);
		_tRotation = TQuaternion(0, 0, 0, 1);
	};

	void C3dObject::SetMesh(C3dMesh* AMesh)
	{
		_pMesh = AMesh;
	};
	
	E_RESOURCE_TYPE C3dObject::GetResourceType()
	{
		return ERT_3D_OBJECT;
	};
};
