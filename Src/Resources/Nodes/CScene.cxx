#include "CScene.hxx"
#include "MEnTypes.hxx"
#include "CRender.hxx"
#include "CCamera.hxx"
#include "CGame.hxx"
#include "CWindow.hxx"
#include "CCore.hxx"

namespace MEn
{
	CScene::CScene()
	{
		_bPresented = false;
		_bPaused = false;
	};

	CScene::~CScene()
	{

	};

	void CScene::Init()
	{
	};

	void CScene::Process()
	{

	};

	CGame* CScene::GetOwnerGame()
	{
		return _OwnerGame;
	};

	void CScene::SetActiveCamera(CCamera* ACamera)
	{
		_ActiveCamera = ACamera;
		if (!_ActiveCamera)
			CCore::GetInstance()->AddToLog("Setting NULL active camera!", ELMT_WARNING);
		else
		{
			_ActiveCamera->SetAspectRatio((float)_OwnerGame->GetParentWindow()->GetWidth() / (float)_OwnerGame->GetParentWindow()->GetHeight());
			CRender::GetInstance()->SetCameraTransform(_ActiveCamera->GetTransform(true));
		};
	};

	CCamera* CScene::GetActiveCamera()
	{
		return _ActiveCamera;
	}

	bool CScene::KeyboardCallback(E_KEYBOARD_KEYS AKey, E_MODIFIERS AModifier, E_KEY_STATE AStste)
	{
		return true;
	};

	bool CScene::MouseButtonCallback(E_MOUSE_BUTTONS AButton, E_MODIFIERS AModifiers, E_BUTTON_STATE AState)
	{
		return true;
	};

	bool CScene::MouseWheelScrollCallback(double AXOffset, double AYOffset)
	{
		return true;
	};

	bool CScene::CursorPositionCallback(double AX, double AY)
	{
		return true;
	};

	E_RESOURCE_TYPE CScene::GetResourceType()
	{
		return ERT_SCENE;
	};

	bool CScene::Paused()
	{
		return _bPaused;
	};

	bool CScene::Presented()
	{
		return _bPresented;
	};

	void CScene::Presented(bool APresented, CGame* AnOwnerGame)
	{
		_bPresented = APresented;
		_OwnerGame = AnOwnerGame;
	};

	void CScene::SetPaused(bool APaused)
	{
		_bPaused = APaused;
	};
};