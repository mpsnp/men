#include <algorithm>
#include "CNode.hxx"
#include "CCore.hxx"

namespace MEn
{
	void CNode::Render()
	{
		for (CNode* child : _vChildren)
			child->Render();
	};

	void CNode::AddChild(CNode* AChild)
	{
		AChild->SetParent(this);
		_vChildren.push_back(AChild);
	};

	void CNode::RemoveChild(CNode* AChild)
	{
		auto forRemove = find(_vChildren.begin(), _vChildren.end(), AChild);
		_vChildren.erase(forRemove);
		AChild->SetParent(NULL);
	};

	CNode* CNode::GetParent()
	{
		return _pParent;
	};

	void CNode::SetParent(CNode* AParent)
	{
		_pParent = AParent;
	};
};