#include "CGame.hxx"
#include "CCore.hxx"
#include "CScene.hxx"

namespace MEn
{
	void CGame::Init()
	{
		_pCurrentScene = NULL;
	};

	void CGame::Process()
	{
		if (!_pCurrentScene->Paused())
			_pCurrentScene->Process();
	};

	void CGame::Render()
	{
		_pCurrentScene->Render();
	};

	void CGame::SetParentWindow(CWindow* AWindow)
	{
		_pParentWindow = AWindow;
	};

	CWindow* CGame::GetParentWindow()
	{
		return _pParentWindow;
	};

	CScene* CGame::GetCurrentScene()
	{
		return _pCurrentScene;
	};

	void CGame::Free()
	{
	};

	bool CGame::KeyboardCallback(E_KEYBOARD_KEYS AKey, E_MODIFIERS AModifier, E_KEY_STATE AStste)
	{
		_pCurrentScene->KeyboardCallback(AKey, AModifier, AStste);
		return true;
	};

	bool CGame::MouseButtonCallback(E_MOUSE_BUTTONS AButton, E_MODIFIERS AModifiers, E_BUTTON_STATE AState)
	{
		_pCurrentScene->MouseButtonCallback(AButton, AModifiers, AState);
		return true;
	};

	bool CGame::MouseWheelScrollCallback(double AXOffset, double AYOffset)
	{
		_pCurrentScene->MouseWheelScrollCallback(AXOffset, AYOffset);
		return true;
	};

	bool CGame::CursorPositionCallback(double AX, double AY)
	{
		_pCurrentScene->CursorPositionCallback(AX, AY);
		return true;
	};

	E_RESOURCE_TYPE CGame::GetResourceType()
	{
		return ERT_GAME;
	};

	void CGame::SetCurrentScene(CScene* AScene)
	{
		if (AScene->Presented())
			CCore::GetInstance()->AddToLog("Trying to present already presented scene!", ELMT_WARNING);
		else
		{
			if (_pCurrentScene)
				_pCurrentScene->Presented(false, NULL);
			_pCurrentScene = AScene;
			_pCurrentScene->Presented(true, this);
			_pCurrentScene->Init();
		}
	};

	bool CGame::Running()
	{
		return _bRunning;
	};

	void CGame::Running(bool IsRunning)
	{
		_bRunning = IsRunning;
	};
};