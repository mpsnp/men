#include "CShaderProgramManipulator.hxx"
#include "CCore.hxx"
#include "CShader.hxx"
#include "CResourceManager.hxx"
#include "CShaderProgram.hxx"
#include <string>
#include <cstring>
#include <fstream>

#define EXT_LENGTH 3

bool MEn::CShaderProgramManipulator::CanLoadResource(std::string AFileName)
{
	AFileName = AFileName.substr(AFileName.length() - EXT_LENGTH, EXT_LENGTH);
	const char* ext = AFileName.c_str();
	return (strcmp(ext, "shp") == 0);
};

MEn::CResource* MEn::CShaderProgramManipulator::LoadResourceFromFile(std::string AFileName)
{
	CCore::GetInstance()->AddToLog("Loading shader program: " + AFileName, ELMT_NOTE);

	CShaderProgram* program = nullptr;
	std::ifstream input;
	input.open(AFileName);
	if (input.is_open())
	{
		std::string shaderPath;
		program = new CShaderProgram();
		CCore* core = CCore::GetInstance();
		CResourceManager* resourceMan = dynamic_cast<CResourceManager*>(core->GetSubSystem(EST_RESOURCE_MANAGER));

		while (!input.eof())
		{
			input >> shaderPath;

			CShader* shader = dynamic_cast<CShader*>(resourceMan->LoadResourceFromFile(shaderPath));
			if (shader)
				program->AddShader(shader);
			else
			{
				core->AddToLog("Stopping loading shader program.", ELMT_WARNING);
				delete program;
				program = nullptr;
				break;
			}
		};

		if(program)
			core->AddToLog("Shader program loaded successfully.", ELMT_NOTE);

		input.close();
	};
	return program;
};
