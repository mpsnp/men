#include "C3dObjManipulator.hxx"
#include "MEnStructs.hxx"
#include "C3dMesh.hxx"
#include "CCore.hxx"
#include <fstream>
#include <vector>
#include <cstring>

#define EXT_LENGTH 3

namespace MEn
{
	bool C3dObjManipulator::CanLoadResource(std::string AFileName)
	{
		AFileName = AFileName.substr(AFileName.length() - EXT_LENGTH, EXT_LENGTH);
		const char* ext = AFileName.c_str();
		return (strcmp(ext, "obj") == 0);
	}

	CResource* C3dObjManipulator::LoadResourceFromFile(std::string AFileName)
	{
		C3dMesh* result = NULL;

		std::fstream inputFile;
		inputFile.open(AFileName);

		if (!inputFile.is_open())
			CCore::GetInstance()->AddToLog("Could not open file " + AFileName, ELMT_ERROR);
		else
		{
			char ch = 0;
			std::vector <TVertex3f> vertexes;
			std::vector <TFace> faces;
			char tempString[100];
			result = new C3dMesh();

			// читаем строку
			while (!inputFile.eof())
			{
				inputFile >> ch;
				// проблема в том, что мы не знаем сколько там вершин и треугольников
				TVertex3f tempVertex;
				TFace tempFace;
				switch (ch)
				{
				case 'v':
					inputFile >> tempVertex.x >> tempVertex.y >> tempVertex.z;
					vertexes.push_back(tempVertex);
					break;
				case 'f':
					{
						inputFile >> tempFace.v1 >> tempFace.v2 >> tempFace.v3;
						tempFace.v1--;
						tempFace.v2--;
						tempFace.v3--;
						faces.push_back(tempFace);
						break;
					};
				default:
					inputFile.getline(tempString, 100);
					break;
				}
			}

			CCore::GetInstance()->AddToLog("Succsessfull read", ELMT_NOTE);
			TVertex3f* a = new TVertex3f[vertexes.size()];
			for (int i = 0; i < vertexes.size(); i++)
				a[i] = vertexes[i];
			result->SetVertexes(a, vertexes.size());

			TFace* b = new TFace[faces.size()];
			for (int i = 0; i < faces.size(); i++) 
				b[i] = faces[i];
			result->SetFaces(b, faces.size());
			inputFile.close();
		};
		return result;
	};
};