#include "CShaderManipulator.hxx"
#include "CShader.hxx"
#include "CCore.hxx"
#include <fstream>
#include <cstring>

#define EXT_LENGTH 2

bool MEn::CShaderManipulator::CanLoadResource(std::string AFileName)
{
	AFileName = AFileName.substr(AFileName.length() - EXT_LENGTH, EXT_LENGTH);
	const char* ext = AFileName.c_str();
	return (strcmp(ext, "vs") == 0) || (strcmp(ext, "fs") == 0) || (strcmp(ext, "gs") == 0);
};

int MEn::CShaderManipulator::_GetLength(std::ifstream &AInput)
{
	AInput.seekg(0, std::ios::end);
	int length = AInput.tellg();
	AInput.seekg(0, std::ios::beg);
	return length;
}

GLenum MEn::CShaderManipulator::_GetType(std::string AFileName)
{
	GLenum result = 0;
	AFileName = AFileName.substr(AFileName.length() - EXT_LENGTH, EXT_LENGTH);
	const char* ext = AFileName.c_str();
	if (strcmp(ext, "vs") == 0)
		result  = GL_VERTEX_SHADER;
	if (strcmp(ext, "fs") == 0)
		result  = GL_FRAGMENT_SHADER;
	if (strcmp(ext, "gs") == 0)
		result  = GL_GEOMETRY_SHADER;
	return result;
};

MEn::CResource* MEn::CShaderManipulator::LoadResourceFromFile(std::string AFileName)
{
	CCore::GetInstance()->AddToLog("Loading shader: " + AFileName, ELMT_NOTE);
	CShader* result = nullptr;
		
	std::ifstream input;
	input.open(AFileName, std::ios_base::in | std::ios_base::binary);
	
	if (!input.is_open())
		CCore::GetInstance()->AddToLog("Could not find shader!", ELMT_FATAL);
	else
	{
		int length = _GetLength(input) + 1;
		char* source = new char[length];
		input.read(source, length - 1);
		source[length - 1] = 0;

		input.close();
		
		result = new CShader(&source, length, _GetType(AFileName));
		CCore::GetInstance()->AddToLog("Succsessfully loaded: " + AFileName, ELMT_NOTE);
	};
	
	return result;
};
