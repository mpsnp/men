#include "CRender.hxx"
#include "CCore.hxx"
#include "MEnGL.hxx"
#include "CShader.hxx"
#include "CShaderProgram.hxx"
#include "C3dMesh.hxx"
#include <string>

namespace MEn
{
	CRender* CRender::_pSingle = nullptr;

	CRender::CRender()
	{
		std::string version("OpenGL version: ");
		version += (char*)glGetString(GL_VERSION);
		CCore::GetInstance()->AddToLog(version, ELMT_NOTE);

		version.clear();
		version += (char*)glGetString(GL_SHADING_LANGUAGE_VERSION);
		_cGLSLVersion = std::stof(version);
		CCore::GetInstance()->AddToLog("GLSL version: " + version, ELMT_NOTE);

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);
	};

	CRender::~CRender()
	{

	};

	GLuint CRender::GetAttributeLocation(E_ATTRIBUTE_TYPE AnAttribute)
	{
		switch (AnAttribute)
		{
			case EAT_VERTEX:
				return glGetAttribLocation(_ActiveShaderProgram->GetShaderProgramID(), "VertexPosition");
		};
	};

	void CRender::UseShaderProgram(CShaderProgram* AProgram)
	{
		_ActiveShaderProgram = AProgram;
		glUseProgram(_ActiveShaderProgram->GetShaderProgramID());
		_MVPLocation = glGetUniformLocation(_ActiveShaderProgram->GetShaderProgramID(), "MVP");
	};

	void CRender::SetCameraTransform(TMatrix4f* ACameraTransform)
	{
		_CameraTransform = ACameraTransform;
	};

	void CRender::SetModelTransform(TMatrix4f* AModelTransform)
	{
		_ModelTransform = AModelTransform;
	};

	void CRender::Render(CResource* AResource)
	{
		C3dMesh* res = dynamic_cast<C3dMesh*>(AResource);
		if (!res)
			CCore::GetInstance()->AddToLog("Trying to render not graphics resource.", ELMT_FATAL);
		else
		{
			_MVP = (*_CameraTransform) * (*_ModelTransform);
			glUniformMatrix4fv(_MVPLocation, 1, GL_TRUE, (const GLfloat*)&_MVP);
			res->Render();
		};
	};
	
	CRender* CRender::GetInstance()
	{
		if (!_pSingle)
			_pSingle = new CRender();
		return _pSingle;
	};

	void CRender::FreeInstance()
	{
		if (_pSingle)
		{
			delete _pSingle;
			_pSingle = nullptr;
		};
	};

	void CRender::LoadResourceToGPU(CResource* AResource)
	{
		CGraphicsResource* res = dynamic_cast<CGraphicsResource*>(AResource);
		if (!res)
			CCore::GetInstance()->AddToLog("Specified resource is not graphics resource!", ELMT_FATAL);
		else
			if (!res->LoadToGPU())
				CCore::GetInstance()->AddToLog("Could not load resource to GPU!", ELMT_FATAL);
	};

	E_SUBSYSTEM_TYPE CRender::GetType()
	{
		return EST_RENDER;
	};
};