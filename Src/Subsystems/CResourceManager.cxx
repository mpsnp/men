#include "CResourceManager.hxx"
#include "C3dObjManipulator.hxx"
#include "CShaderManipulator.hxx"
#include "CShaderProgramManipulator.hxx"
#include "CCore.hxx"

namespace MEn
{
	CResourceManager::CResourceManager():CSubSystem()
	{
		Init();
		RegisterResourceManipulator(new C3dObjManipulator());
		RegisterResourceManipulator(new CShaderManipulator());
		RegisterResourceManipulator(new CShaderProgramManipulator());
	};

	CResourceManager::~CResourceManager()
	{
		Free();
	};
	
	void CResourceManager::Free()
	{
		for (auto manipulator : _vManipulators)
			if (manipulator)
				delete manipulator;
		for (auto resource : _vResources)
			if (resource)
				delete resource;
	};

	void CResourceManager::RegisterResourceManipulator(CResourceManipulator* AManipulator)
	{
		_vManipulators.push_back(AManipulator);
	};

	CResource* CResourceManager::LoadResourceFromFile(std::string AFileName)
	{
		CResource* resource = NULL;
		for (auto manipulator : _vManipulators)
			if (manipulator->CanLoadResource(AFileName))
			{
				resource = manipulator->LoadResourceFromFile(AFileName);
				break;
			};
		if (!resource)
			CCore::GetInstance()->AddToLog("Resource manipulator is not found, or didn't work properly!", ELMT_ERROR);
		return resource;
	};

	void CResourceManager::SaveResourceToFile(CResource* AResource, std::string AFileName)
	{
		bool found = false;
		for (auto manipulator : _vManipulators)
			if (manipulator->CanSaveResource(AResource))
			{
				manipulator->SaveResourceToFile(AResource, AFileName);
				found = true;
				break;
			};
		if (!found)
			CCore::GetInstance()->AddToLog("Resource manipulator not found", ELMT_ERROR);
	};
};