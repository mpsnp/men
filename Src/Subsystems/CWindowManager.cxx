#include "CWindowManager.hxx"
#include "CCore.hxx"
#include "MEnGL.hxx"
#include <algorithm>

namespace MEn
{
	CWindowManager::CWindowManager():CSubSystem()
	{
		Init();
	};

	CWindowManager::~CWindowManager()
	{
		for (CWindow* window : _pWindows)
			if (window)
				delete window;
	};

	void CWindowManager::_ErrorCallback(int AError, const char* ADescription)
	{
		CCore::GetInstance()->AddToLog(ADescription, ELMT_ERROR);
	};

	void CWindowManager::Init()
	{
		glfwSetErrorCallback(_ErrorCallback);
		if (!glfwInit())
			CCore::GetInstance()->AddToLog("Cannot init window manager", ELMT_FATAL);
		CCore::GetInstance()->AddToLog("Window manager succsessfull init!", ELMT_NOTE);
	};

	void CWindowManager::Free()
	{
		for (CWindow* i: _pWindows)
			delete i;
		_pWindows.clear();
		glfwTerminate();
	};

	CWindow* CWindowManager::CreateWindow(int AWidth, int AHeight, std::string ATitle, E_WINDOW_INIT_FLAGS AFlags)
	{
		CWindow* window = CWindow::CreateWindow(AWidth, AHeight, ATitle, AFlags);
		_pWindows.push_back(window);
		return window;
	};

	E_SUBSYSTEM_TYPE CWindowManager::GetType()
	{
		return EST_WINDOW_MANAGER;
	};

	void CWindowManager::DeleteWindow(CWindow* AWindow)
	{
		auto window = std::find(_pWindows.begin(), _pWindows.end(), AWindow);
		_pWindows.erase(window);
		delete *window;
		AWindow = NULL;
	};
};