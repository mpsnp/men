#include "CSubSystem.hxx"
#include "CCore.hxx"

namespace MEn
{
	void CSubSystem::Init()
	{
	};

	void CSubSystem::Free()
	{
	};

	E_SUBSYSTEM_TYPE CSubSystem::GetType()
	{
		return EST_UNKNOWN;
	};

	CSubSystem::CSubSystem()
	{
		Init();
	};

	CSubSystem::~CSubSystem()
	{
		Free();
	};
};