#include "MEn.hxx"
#include "MEnGL.hxx"

using namespace MEn;

#define WIDTH 1024
#define HEIGHT 768
#define DEFAULT_CAMERA_SPEED 0.05
#define HIGH_CAMERA_SPEED 0.15

class CTestScene : public CScene
{
	private:
		bool _Rotating;
		bool _MoveForward;
		bool _MoveBackward;
		bool _MoveRight;
		bool _MoveLeft;
		float _CameraSpeed;
		C3dObject* _Object;
		CCamera* _Camera;
	public:
		CTestScene()
		{
			_Rotating = true;
			_MoveForward = false;
			_MoveBackward = false;
			_MoveLeft = false;
			_MoveRight = false;
			_CameraSpeed = DEFAULT_CAMERA_SPEED;
		};
	
		~CTestScene(){};

		bool MouseButtonCallback(MEn::E_MOUSE_BUTTONS AButton, MEn::E_MODIFIERS AModifiers, MEn::E_BUTTON_STATE AState)
		{
			if (AButton == EMB_LEFT)
				_Rotating = (AState == EBS_RELEASED);
			return true;
		};

		bool CursorPositionCallback(double AX, double AY)
		{
			float width = GetOwnerGame()->GetParentWindow()->GetWidth();
			float height = GetOwnerGame()->GetParentWindow()->GetHeight();
			_Camera->Yaw((AX - width / 2) / width);
			_Camera->Pitch(-(AY - height / 2) / height);
			return true;
		};

		bool KeyboardCallback(MEn::E_KEYBOARD_KEYS AKey, MEn::E_MODIFIERS AModifier, MEn::E_KEY_STATE AStste)
		{
			if (AKey == EKK_W)
				_MoveForward = (AStste != EKS_RELEASED);
			if (AKey == EKK_S)
				_MoveBackward = (AStste != EKS_RELEASED);
			if (AKey == EKK_A)
				_MoveLeft = (AStste != EKS_RELEASED);
			if (AKey == EKK_D)
				_MoveRight = (AStste != EKS_RELEASED);
			if ((AKey == EKK_Q) && (AStste == EKS_PRESSED))
			{
				const float alpha = M_PI / 6;
				_Object->Rotate(TQuaternion(TVertex3f(1, 0, 1).Normalize() * sinf(alpha / 2), cosf(alpha / 2)));
			}
			if (AKey == EKK_LEFT_SHIFT)
			{
				if (AStste != EKS_RELEASED)
					_CameraSpeed = HIGH_CAMERA_SPEED;
				else
					_CameraSpeed = DEFAULT_CAMERA_SPEED;
			};
			
			return true;
		};

		void Process()
		{
			if (_MoveForward)
				_Camera->MoveForward(_CameraSpeed);
			if (_MoveBackward)
				_Camera->MoveForward(-_CameraSpeed);
			if (_MoveRight)
				_Camera->MoveRight(_CameraSpeed);
			if (_MoveLeft)
				_Camera->MoveRight(-_CameraSpeed);
		};
	
		void Init()
		{
			CScene::Init();
			CResourceManager* rm = (CResourceManager*)CCore::GetInstance()->GetSubSystem(EST_RESOURCE_MANAGER);
			
			C3dMesh* mesh = (C3dMesh*)rm->LoadResourceFromFile("Data/test.obj");
			CShaderProgram* program = (CShaderProgram*)rm->LoadResourceFromFile("Data/DefaultShaderProgram.shp");
			
			CRender::GetInstance()->LoadResourceToGPU(program);
			CRender::GetInstance()->UseShaderProgram(program);
		
			_Object = new C3dObject();
			_Object->SetMesh(mesh);
			this->AddChild(_Object);

			_Camera = new CCamera();
			this->AddChild(_Camera);
			SetActiveCamera(_Camera);
		};
};

class TestGame : public CGame
{
	private:
		CTestScene* Scene;
	public:
		TestGame(){};
		~TestGame(){};
		void Init()
		{
			CGame::Init();
			Scene = new CTestScene();
			SetCurrentScene(Scene);
			Running(true);
		};

		void Process()
		{
			GetParentWindow()->SetCursorPos(GetParentWindow()->GetWidth() / 2, GetParentWindow()->GetHeight() / 2);
			CGame::Process();
		};

		bool KeyboardCallback(MEn::E_KEYBOARD_KEYS AKey, MEn::E_MODIFIERS AModifier, MEn::E_KEY_STATE AStste)
		{
			CGame::KeyboardCallback(AKey, AModifier, AStste);
			if ((AKey == EKK_ESCAPE) && (AStste == EKS_RELEASED))
				Running(false);
			return true;
		};
};

int main(int argc, char const *argv[])
{
	CCore* core = CCore::GetInstance();
	CWindowManager* wm = (CWindowManager*) core->GetSubSystem(EST_WINDOW_MANAGER);
	CWindow* wnd = wm->CreateWindow(WIDTH, HEIGHT, "Test title", EWIF_DEFAULT);
	CGame* game = new TestGame();
	wnd->SetGameInstance(game);
	CCore::FreeInstance();
	return 0;
};