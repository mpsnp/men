#version 120

// Input vertex data, different for all executions of this shader.
attribute vec3 VertexPosition;

// Values that stay constant for the whole mesh.
uniform mat4 MVP;

// Interpolated values from the vertex shaders
varying vec3 fragmentColor;

void main(){	

	// Output position of the vertex, in clip space : MVP * position
	gl_Position = MVP * vec4(VertexPosition,1);
	fragmentColor.x = abs(VertexPosition.x);
	fragmentColor.y = abs(VertexPosition.y);
	fragmentColor.z = abs(VertexPosition.z);
}

